# outputs.tf specifies outputs from the module

output "value" {
  value     = data.external.secret.result
  sensitive = true
}
