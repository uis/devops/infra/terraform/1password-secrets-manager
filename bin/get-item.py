#!/usr/bin/env python3
import json
import shutil
import subprocess
import sys


def main():
    config = json.load(sys.stdin)
    op = OP(op_path=config.get('opPath'))

    type_ = config.get('type')
    if type_ is None:
        print('type not specified in input', file=sys.stderr)
        sys.exit(1)

    uuid = config.get('uuid')
    if uuid is None:
        print('uuid not specified in input', file=sys.stderr)
        sys.exit(1)

    output = {}
    if type_ == 'api_credential':
        item = op('item', 'get', uuid, "--format", "json")
        # "purpose" is not available for API credentials fields, so use "id"
        username = [
            f for f in item['fields'] if f.get('id', '') == 'username'
        ][0]['value']
        credential = [
            f for f in item['fields'] if f.get('id', '') == 'credential'
        ][0]['value']
        output = {
            'uuid': uuid, 'title': item['title'],
            'username': username, 'credential': credential
        }
    elif type_ == 'document':
        doc = op('document', 'get', uuid, as_json=False)
        output = {'uuid': uuid, 'content': doc}
    elif type_ == 'login':
        item = op('item', 'get', uuid, "--format", "json")
        username = [
            f for f in item['fields'] if f.get('purpose', '') == 'USERNAME'
        ][0]['value']
        password = [
            f for f in item['fields'] if f.get('purpose', '') == 'PASSWORD'
        ][0]['value']
        output = {
            'uuid': uuid, 'title': item['title'],
            'username': username, 'password': password
        }
    elif type_ == 'password':
        item = op('item', 'get', uuid, "--format", "json")
        output = {
            'uuid': uuid, 'title': item['title'],
            'content': [f for f in item['fields'] if f.get('purpose', '') == 'PASSWORD'][0]['value']
        }
    else:
        print(f'Unknown type: {type_!r}', file=sys.stderr)
        sys.exit(1)

    json.dump(output, sys.stdout)


class OP:
    """
    Wrapper for op command line utility.

    """
    def __init__(self, op_path=None):
        if op_path is None:
            op_path = shutil.which('op')
        if op_path is None:
            print('Could not find "op" command on path.', file=sys.stderr)
            sys.exit(1)
        self.op_path = op_path
        version = self.__call__('-v', as_json=False)
        if (int(version[0]) < 2):
            print(f"OP version {version.strip()} is not supported", file=sys.stderr)
            sys.exit(1)

    def __call__(self, *args, as_json=True):
        """
        Call the "op" command passing arguments. Returns stdout.

        Immediately exits the script if the command's exit status is non-zero.

        """
        process = subprocess.run(
            [self.op_path] + list(args), capture_output=True,
            encoding='utf8', errors='ignore')
        if process.returncode != 0:
            sys.stderr.write(process.stderr)
            sys.exit(1)
        if as_json:
            return json.loads(process.stdout)
        return process.stdout


if __name__ == '__main__':
    main()
