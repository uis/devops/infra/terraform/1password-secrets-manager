# 1Password Secrets Manager

This module will fetch secrets saved as documents and passwords from 1Password.

NOTE: 1Password can store other file types (notes, passwords etc).
This module can currently retrieve documents and passwords in 1Password.

This module relies on the 1Password CLI tool [op](https://support.1password.com/command-line-getting-started/)
version 2.x.

This module conforms to the [terraform standard module
structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).

The uuid and content for the document from the project
[outputs](outputs.tf). Configuration is documented in [variables.tf](variables.tf).

## Requirements

* The [1password CLI](https://1password.com/downloads/command-line/) must be available on the current `PATH` as `op`.
* The [jq utility](https://stedolan.github.io/jq/) must be available on the current `PATH` as `jq`.

## Manual bootstrap

Before running terraform that uses this module you wil need to login to 1Password with:

`op account add --address <sign_in_address> --email <email_address> --secret-key <secret_key>` 

`eval $(op signin)`

## Document UUIDs

This module requires that a specific UUID be used to reference 1password secrets.
The UUID can be obtained from the `op` CLI tool in the following manner.

A list of available vaults can be obtained with:

`op vault list`

Then a list of available documents (including UUIDs) can be obtained with:

`op item list --vault <vault_id>`

The content of an item can be checked with:

`op item get <UUID>`

To get the content of a password:

`op item get <UUID> --fields label=password`

## Output schema

See the [1password schema documentation](https://developer.1password.com/docs/cli/upgrade#new-schema)


## Usage Examples

The [examples](examples/) directory contains examples of use. A basic usage
example is available in [examples/root-example](examples/root-example/).
