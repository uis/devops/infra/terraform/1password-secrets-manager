# variables.tf defines variables used by the module

variable "uuid" {
  description = "UUID of item to retrieve from 1Password"
  type        = string
}

variable "type" {
  description = "Type of secret to retrieve. One of {document, password}. Default: document"
  default     = "document"

  validation {
    condition     = contains(["api_credential", "document", "login", "password"], var.type)
    error_message = "Type must be one of {api_credential, document, login, password}."
  }
}
