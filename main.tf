# main.tf defines resources managed by the module

data "external" "secret" {
  program = [local.one_password_script]
  query = {
    "type" = var.type
    "uuid" = var.uuid
  }
}
