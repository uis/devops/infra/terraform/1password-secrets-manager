# Simple example

This is a simple example of retrieving a secret document from 1Password.

```console
$ terraform init
$ terraform apply
```

You should already have the 1Password CLI tool 
[op](https://support.1password.com/command-line-getting-started/) 
installed and should be logged in to 1Password with: 
`eval $(op signin <sign_in_address> <email_address> <secret_key>)`