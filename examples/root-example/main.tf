# main.tf defines resources managed by the module

module "one_password_api_credential" {
  source = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/1password-secrets-manager.git"
  type   = "api_credential"
  uuid   = "lqepumcipqbaqkorzp7ynpnz5e"
}

output "api_credential" {
  value     = module.one_password_api_credential.value
  sensitive = true
}

module "one_password_document" {
  source = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/1password-secrets-manager.git"
  type   = "document"
  uuid   = "j6sul6kn5eldy72cfrmjkwkfue"
}

output "document" {
  value     = lookup(module.one_password_document.value, "content")
  sensitive = true
}

module "one_password_login" {
  source = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/1password-secrets-manager.git"
  type   = "login"
  uuid   = "ndeqth6ngafzmxal3ws5aekg6y"
}

output "login" {
  value     = module.one_password_login.value
  sensitive = true
}

module "one_password_password" {
  source   = "git::https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/1password-secrets-manager.git"
  for_each = toset(["gx5oh44i3bxjn34knhprmgjrye", "4px426bi6olp3h7wzn6h3jqcyu"])

  type = "password"
  uuid = each.value
}

output "password" {
  value     = lookup(module.one_password_password["4px426bi6olp3h7wzn6h3jqcyu"].value, "content")
  sensitive = true
}
