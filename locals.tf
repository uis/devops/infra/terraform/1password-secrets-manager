# locals.tf defines local variables used throughout the module.

locals {
  # Script to retrieve documents from 1Password
  one_password_script = "${path.module}/bin/get-item.py"
}
